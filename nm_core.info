name = NodeMaker Core
description = <p>NodeMaker Core provides sitewide backend functionality.</p><p>App created for <a href="http://nodemaker.com" target="_blank">NodeMaker</a> by <a href="http://themegeeks.com" target="_blank">ThemeGeeks</a>.</p>
core = 7.x
package = NodeMaker Apps
project = nm_core

; Core Modules
dependencies[] = taxonomy
dependencies[] = contact
dependencies[] = image
dependencies[] = menu
dependencies[] = search

; Contrib Modules
dependencies[] = context
dependencies[] = context_ui
dependencies[] = ctools
dependencies[] = features
dependencies[] = strongarm
dependencies[] = field_group
dependencies[] = globalredirect
dependencies[] = navigation404
dependencies[] = strongarm
dependencies[] = transliteration
dependencies[] = views
dependencies[] = views_ui
dependencies[] = menu_expanded
dependencies[] = menu_block
dependencies[] = pathauto
dependencies[] = token
dependencies[] = logintoboggan
dependencies[] = entity
dependencies[] = entityreference
dependencies[] = block_class
dependencies[] = diff
dependencies[] = field_group
dependencies[] = imagecrop
dependencies[] = colorbox
dependencies[] = module_filter
dependencies[] = metatag
dependencies[] = nice_menus
dependencies[] = taxonomy_menu_trails
dependencies[] = captcha
dependencies[] = sharethis

; Context(s)
features[context][] = nm_core
features[context][] = nm_core_nodes
features[context][] = nm_core_403
features[context][] = nm_core_share

; API(s)
features[ctools][] = context:context:3
features[ctools][] = field_group:field_group:1
features[ctools][] = strongarm:strongarm:1
features[ctools][] = views:views_default:3.0
features[features_api][] = api:1

; Field Group(s)
features[field_group][] = group_nm_page_content|node|nm_page|form
features[field_group][] = group_nm_page_info|node|nm_page|form
features[field_group][] = group_nm_page_media|node|nm_page|form
features[field_group][] = group_nm_page_relations|node|nm_page|form

; Menu(s)
features[menu_custom][] = menu-footer-links
features[menu_links][] = menu-footer-links:contact

; Content Type(s)
features[node][] = nm_page

; Vocabulary(s)
features[taxonomy][] = nm_tags

; Permissions(s)
features[user_permission][] = access site-wide contact form
features[user_permission][] = change own username
features[user_permission][] = search content
features[user_permission][] = skip CAPTCHA

; Views(s)
features[views_view][] = nm_related_content

; Image Style(s)
features[image][] = nodemaker_headline_image
features[image][] = nodemaker_thumbnail
features[image][] = nodemaker_thumbnail_small

; Variable(s)
features[variable][] = change_own_username
features[variable][] = colorbox_caption_trim
features[variable][] = colorbox_caption_trim_length
features[variable][] = colorbox_compression_type
features[variable][] = colorbox_custom_settings_activate
features[variable][] = colorbox_fixed
features[variable][] = colorbox_initialheight
features[variable][] = colorbox_initialwidth
features[variable][] = colorbox_inline
features[variable][] = colorbox_load
features[variable][] = colorbox_login
features[variable][] = colorbox_login_links
features[variable][] = colorbox_maxheight
features[variable][] = colorbox_maxwidth
features[variable][] = colorbox_opacity
features[variable][] = colorbox_overlayclose
features[variable][] = colorbox_pages
features[variable][] = colorbox_path
features[variable][] = colorbox_scrolling
features[variable][] = colorbox_slideshow
features[variable][] = colorbox_slideshowauto
features[variable][] = colorbox_slideshowspeed
features[variable][] = colorbox_style
features[variable][] = colorbox_text_close
features[variable][] = colorbox_text_current
features[variable][] = colorbox_text_next
features[variable][] = colorbox_text_previous
features[variable][] = colorbox_text_start
features[variable][] = colorbox_text_stop
features[variable][] = colorbox_transition_speed
features[variable][] = colorbox_transition_type
features[variable][] = comment_anonymous_nm_page
features[variable][] = comment_default_mode_nm_page
features[variable][] = comment_default_per_page_nm_page
features[variable][] = comment_form_location_nm_page
features[variable][] = comment_nm_page
features[variable][] = comment_preview_nm_page
features[variable][] = comment_subject_field_nm_page
features[variable][] = contact_default_status
features[variable][] = enable_revisions_page_nm_page
features[variable][] = field_bundle_settings_node__nm_page
features[variable][] = imagecrop_modules
features[variable][] = imagecrop_popup
features[variable][] = imagecrop_popup_height
features[variable][] = imagecrop_popup_width
features[variable][] = imagecrop_rotation
features[variable][] = imagecrop_scale_default
features[variable][] = imagecrop_scale_step
features[variable][] = imagecrop_show_cancel_button
features[variable][] = imagecrop_skip_preview
features[variable][] = imagecrop_theme
features[variable][] = imagecrop_ui_controls
features[variable][] = logintoboggan_confirm_email_at_registration
features[variable][] = logintoboggan_immediate_login_on_register
features[variable][] = logintoboggan_login_successful_message
features[variable][] = logintoboggan_login_with_email
features[variable][] = logintoboggan_minimum_password_length
features[variable][] = logintoboggan_override_destination_parameter
features[variable][] = logintoboggan_pre_auth_role
features[variable][] = logintoboggan_purge_unvalidated_user_interval
features[variable][] = logintoboggan_redirect_on_confirm
features[variable][] = logintoboggan_redirect_on_register
features[variable][] = logintoboggan_unified_login
features[variable][] = menu_options_nm_page
features[variable][] = menu_parent_nm_page
features[variable][] = menu_secondary_links_source
features[variable][] = node_admin_theme
features[variable][] = node_options_nm_page
features[variable][] = node_preview_nm_page
features[variable][] = node_submitted_nm_page
features[variable][] = pathauto_case
features[variable][] = pathauto_nm_core_profile_pattern
features[variable][] = pathauto_nm_core_activity_pattern
features[variable][] = pathauto_nm_core_blog_pattern
features[variable][] = pathauto_nm_core_contact_pattern
features[variable][] = pathauto_nm_core_edit_pattern
features[variable][] = pathauto_nm_core_pattern
features[variable][] = pathauto_node_nm_page_pattern
features[variable][] = pathauto_node_pattern
features[variable][] = pathauto_punctuation_hyphen
features[variable][] = pathauto_punctuation_underscore
features[variable][] = pathauto_separator
features[variable][] = pathauto_transliterate
features[variable][] = pathauto_update_action
features[variable][] = pathauto_user_pattern
features[variable][] = sharethis_button_option
features[variable][] = sharethis_location
features[variable][] = sharethis_node_option
features[variable][] = sharethis_option_extras
features[variable][] = sharethis_service_option
features[variable][] = sharethis_teaser_option
features[variable][] = sharethis_twitter_suffix
features[variable][] = sharethis_weight
features[variable][] = sharethis_widget_option
features[variable][] = show_diff_inline_nm_page
features[variable][] = show_preview_changes_nm_page
features[variable][] = user_pictures
features[variable][] = user_register
features[variable][] = sharethis_button_option
features[variable][] = sharethis_location
features[variable][] = sharethis_service_option